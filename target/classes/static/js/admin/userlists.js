layui.use(['table','form'],function () {
    let $ = layui.$,
        form = layui.form,
        table  = layui.table;
    let delVal = $("body").attr("userId");
    let tableData = table.render({
        elem: '#demo',
        title:'管理员列表'
        ,toolbar:true
        ,even:false
        ,url: '/admin/getData' //数据接口
        ,page: true //开启分页
        ,cols: [[ //表头
            {field: 'id', title: 'ID', width:80, sort: true, fixed: 'left'}
            ,{field: 'devCode', title: '用户名'}
            ,{field: 'devPwd', title: '密码'}
            ,{field: 'devName', title: '昵称' }
            ,{field: 'devEmail', title: '邮箱'}
            ,{field: 'devPhone', title: '手机'}
            ,{field: 'createDate', title: '创建时间', sort: true}
            ,{field: 'operate', title: '操作',templet:'#operate'}
        ]],
        limit:5,
        limits:[5,10,15,20]
    });
    //搜索
    form.on('submit(sreach)',function (d) {
        tableData.reload({
            where: d.field
            ,page: {
                curr: 1
            }
        });
    });
    let addDevUserWindow;
    //添加管理员弹框
    $('#addUserBtn').click(function () {
        addDevUserWindow = layer.open({
            type:1,
            title:'添加管理员',
            skin:'layui-layer-lan',
            content:$("#addUserBox")
        });
    });
    //自定义验证
    form.verify({
        isDevCode:function(val,ele){
            let res = "";
            $.ajax({
                url:'/admin/isCode/'+val,
                type:"get",
                async:false,
                success:function (data) {
                    if(data=="true"||data){
                        res = "用户名已存在！";
                    }
                },error:function () {
                    res = "请求异常！";
                }
            });
            return res;
        },
        format:function (val,ele) {
            if(!new RegExp("^[A-Za-z0-9]+$").test(val)){
                return '只能输入英文和数字';
            }
        },
        length:function (val,ele) {
            if(!new RegExp('^[\\S]{6,12}$').test(val)){
                return '长度必须6到12位';
            }
        },
        string:function (val,ele) {
            if(!val.match(/^[\u4e00-\u9fa5]{2,4}$/)){
                return "只能输入2-4位字符";
            }
        }
    });
    //添加
    form.on('submit(addDevUser)',function (data) {
        $.ajax({
            url:"/admin/addDevUser",
            type:"post",
            data:data.field,
            sync:false,
            success:function (data) {
                if(data=="true"||data){
                    layer.msg('添加成功!',{icon:6,anim:1,time:1000});
                }else{
                    layer.msg('添加失败!',{icon:5,anim:1,time:1000})
                }
            }
        });
        layer.close(addDevUserWindow);
        $("#addDevUser")[0].reset();
        tableData.reload();
        return false;
    });
    //监听数据行
    let changeUserWindow;
    table.on('tool(table)',function (obj) {
        if(obj.event === 'edit'){
            form.val('changeUser',obj.data);
            changeUserWindow = layer.open({
                type:1,
                title:'修改管理员信息',
                skin:'layui-layer-lan',
                content:$("#ChangeUserBox")
            });
        }else if(obj.event === 'del'){
            if(obj.data.id == delVal){
                layer.alert("不能删除当前用户！",{icon:2,anim:5,title:'提示',time:1300});
            }else{
                let delConfirm = layer.confirm("确定删除"+obj.data.devCode+"吗?",{btn:['确定','取消'],title:'提示'},function () {
                    $.get("/admin/delUser/"+obj.data.id,function (data) {
                        if(data=="true"||data){
                            layer.msg('删除成功！',{icon:1,time:1200});
                        }else{
                            layer.msg('删除失败!',{icon:2,time:1200});
                        }
                        layer.close(delConfirm);
                        tableData.reload();
                    });
                });
            }
        }
    });
    //修改
    form.on('submit(changeUser)',function (fd) {
            $.post("/admin/changeUser",fd.field,function (data) {
                if(data=="true"||data){
                    if(fd.field.id == delVal){
                        layer.alert('您修改了当前用户信息,请重新登录!',{title:'修改成功',icon:7,anim:1,time:2000,end:function(){
                            window.parent.location.href="/admin/exitSys";
                        }});
                    }else{
                        layer.msg('修改成功!',{icon:6,time:1000});
                    }
                }else{
                    layer.msg('修改失败!',{icon:5,anim:5,time:1000})
                }
            });
        layer.close(changeUserWindow);
        tableData.reload();
        return false;
    });

});