layui.use(['form','table'],function () {
    let form = layui.form,
        $  = layui.$,
        table = layui.table;

    let appTable = table.render({
        elem:"#table",
        url:'/app/getUserAppLists',
        page:true,
        title:'App信息列表',
        cols:[
            [
                {field: 'id', title: 'ID', width:80, sort: true},
                {field: 'appName', title: '软件名称'},
                {field: 'type', title: '状态',templet:function (d) {
                        return d.type==1?'已上架':'未上架';
                    }},
                {field: 'sortOne', title: '分类',templet:function (d) {
                        return d.sort.sortOnestr;
                }},
                {field: 'apkSize',title: '大小',sort: true},
                {field: 'apkUrl',title: '地址',templet:function (d) {
                        return '<a href="'+d.apkUrl+'" style="color:blue">下载</a>';
                    }},
                {field: 'apkCount',title: '下载次数',sort: true},
                {field: 'createDate',title: '创建时间'},
                {field: 'examine',title: '审核状态',templet:function (d) {
                        let res;
                        if(d.examine==1){
                            res = '<button class="layui-btn layui-btn-sm layui-btn-normal">已审核</button>';
                        }else if (d.examine==2){
                            res = '<button class="layui-btn layui-btn-sm layui-btn-warm">未审核</button>';
                        }else if(d.examine==3){
                            res = '<button class="layui-btn layui-btn-sm layui-btn-danger">未通过</button>';
                        }
                        return res;
                    }},
                {field: 'lookerId', title: '审核员',templet:function (d) {
                        return d.devUser.devName==null?'':d.devUser.devName;
                    }},
                {field: 'modifyDate',title: '修改时间'},
                {field: 'operate',title: '操作',templet:function (d) {
                        return "<button class='layui-btn layui-btn-sm layui-btn-normal' lay-event='show'>查看详情</button>";
                    }}
            ]
        ],
        limit:5,
        limits:[5,10,15,20]
    });

    //渲染下拉框
    $.get("/app/getAllSortOne",function (data) {
        $.each(data,function (index,value) {
            $("#sortOne").append('<option value="'+data[index].id+'">'+data[index].sortOnestr+'</option>');
        });
        form.render('select');
    });

    //查询
    form.on('submit(formDemo)',function (fd) {
        appTable.reload({
            where:fd.field,
            page: {
                curr:1
            }
        });
    });
    //软件详情页
    let appShowPage;
    //监听table
    table.on('tool(table)',function (obj) {
        if(obj.event=="show"){
            form.val('appShowPage',obj.data);
            $("#appShowPage-type").val(obj.data.type==0?'未上架':'已上架');
            $("#appShowPage-sortOne").val(obj.data.sort.sortOnestr);
            $('textarea[name="context"]').text(obj.data.content);
            $(".ex").attr('index',obj.data.id);
            appShowPage = layer.open({
                title:'软件详情',
                anim:2,
                skin:'layui-layer-lan',
                type:1,
                content:$("#appShowPage")
            });
        }
        //监听[审核]
        $(".ex").on('click',function () {
            $.get("/app/examine?id="+$(this).attr('index')+"&examine="+$(this).attr('name'),function (data) {
                if(data=="true"||data){
                    layer.msg('操作成功！',{icon:1,offset:'t'});
                }else{
                    layer.msg('操作失败！',{icon:2,offset: 't'});
                }
                layer.close(appShowPage);
                appTable.reload();//重新渲染
            });
        });
    });



});