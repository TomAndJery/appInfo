package top.xiaoge;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import top.xiaoge.mapper.AppMapper;
import top.xiaoge.pojo.App;
import java.util.List;
import java.util.UUID;


@SpringBootTest
class AppinfomationApplicationTests {
    @Autowired
    AppMapper appMapper;
    @Test
    void contextLoads() {
        List<App> userApp = appMapper.getUserApp(1, 1, 10, null, null, 2, null);
        //int count = appMapper.getAppCount(1, null, null, null, null);
        System.out.println(userApp);
    }
    @Test
    void uuidTest(){
        App one = appMapper.getOne(1);
        System.out.println(one);
    }
}
