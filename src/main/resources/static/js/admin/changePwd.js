
//加载模块
layui.use(['form','layer'],function () {
    let form = layui.form,
        $ = layui.jquery,
        layer = layui.layer;
    form.verify({
        isExists:function (val,ele) {
            if(val!=ele.getAttribute("pass")){
                return "旧密码不正确";
            }
        },
        //密码验证
        userPwd:function (val,ele) {
            if($("#newPwd").val()!=val){
                return '两次密码输入不一致';
            }
        },
        //格式验证
        format:function (val,ele) {
            if(!new RegExp("^[A-Za-z0-9]+$").test(val)){
                return '只能输入英文和数字';
            }
        },
        //密码长度验证
        pwd:function (val,ele) {
            if(!new RegExp('^[\\S]{6,12}$').test(val)){
                return '密码必须6到12位';
            }
        }
    });

    form.on('submit(*)',function (data) {
        if(data.field.devCode==$(".verifyPwd").attr("pass")){
            layer.alert("新密码不能与旧密码相同",{title:'提示',icon:5,time:1200});
        }else{
            $.post("/admin/changePwd/"+data.field.devCode,function (data) {
                if(data=="true"||data){
                    layer.msg('密码修改成功,前往重新登陆...',{title:'密码修改成功!',time:'1000',icon:1},function () {
                        window.parent.location.href="/admin/exitSys";
                    });
                }else{
                    layer.msg('密码修改失败！',{title:'密码修改失败!',time:'1000',icon:2,anim:5});
                    $("#form")[0].reset();
                }
            });
        }
        return false;
    });

});
