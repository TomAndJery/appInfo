package top.xiaoge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import top.xiaoge.pojo.Version;
import top.xiaoge.service.VersionService;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

@RestController
@RequestMapping("/version")
public class VersionController {

    @Autowired
    VersionService versionService;

    //删除版本
    @RequestMapping("/delVersion")
    public boolean delVersion(Integer appId,Integer id){
        return versionService.delVersion(appId,id);
    }

    //获取历史版本
    @RequestMapping("/gethistory")
    public HashMap<String,Object> getHistory(Integer appId){
        return versionService.getAllVersion(appId);
    }

    //添加版本信息
    @PostMapping("/addVersion")
    public boolean addVersion(@RequestParam("filev") MultipartFile file, Version version, HttpServletRequest request){
        boolean flag = false;
        //如果文件不是空
        if(!file.isEmpty()){
            version.setApkSize(((file.getSize()/1024)/1024));
            try {
                String path = "E:\\upload\\";
                File pathFile = new File(path);
                if(!pathFile.exists()){
                    pathFile.mkdirs();
                }
                String fileName = file.getOriginalFilename();
                String suffixName = fileName.substring(fileName.lastIndexOf('.'));
                String prefixName = fileName.substring(0,fileName.lastIndexOf('.'));
                String newFileName = prefixName+ UUID.randomUUID().toString().substring(8, 13)+suffixName;
                //保存文件
                file.transferTo(Paths.get(new File(path + newFileName)+""));
                version.setCreateDate(new Date());
                version.setApkUrl("http://localhost:"+request.getServerPort()+"/app/down/"+newFileName);
                flag = versionService.addVersion(version);
            }catch (Exception e){
                System.out.println(e.getMessage());
                flag = false;
            }
        }
        return flag;
    }

}
