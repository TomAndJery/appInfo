package top.xiaoge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import top.xiaoge.pojo.BackUser;
import top.xiaoge.pojo.DevUser;
import top.xiaoge.service.BackUserService;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

@Controller
@RequestMapping("/user")
public class BackUserController {
    @Autowired
    BackUserService backUserService;

    //页面
    @GetMapping("/pages/{pageName}")
    public String htmlPages(@PathVariable("pageName")String pageName){
        return "exploitationPage/"+pageName.substring(0,pageName.lastIndexOf('.'));
    }

    //获取分页数据
    @GetMapping("/getUserlists")
    @ResponseBody
    public HashMap<String,Object> getuserLists(Integer page, Integer limit,String userCode, String userName){
        return backUserService.getUserLists(page,limit,userCode,userName);
    }
    //登陆
    @GetMapping("/userLogin")
    @ResponseBody
    public boolean UserLogin(String userCode,String userPwd,HttpSession session){
        BackUser backUser = backUserService.userLogin(userCode, userPwd);
        if(backUser!=null){
            session.setAttribute("adminUser",backUser);
            return true;
        }
        return false;
    }


    //添加
    @ResponseBody
    @PostMapping("/addUser")
    public boolean addUser(BackUser backUser,HttpSession session){
        backUser.setCreateBy(((DevUser)session.getAttribute("adminUser")).getId());
        return backUserService.addUser(backUser);
    }

    //添加（开发者注册）
    @ResponseBody
    @PostMapping("/addBackUser")
    public boolean addBackUser(BackUser backUser,HttpSession session){
        return backUserService.addUser(backUser);
    }
    
    //删除
    @ResponseBody
    @GetMapping("/delUser/{id}")
    public boolean delUser(@PathVariable("id")Integer id){
        return backUserService.delUser(id);
    }

    //修改
    @ResponseBody
    @PostMapping("/changeUser")
    public boolean change(BackUser backUser, HttpSession session){
        backUser.setModifyBy(((DevUser)session.getAttribute("adminUser")).getId());
        return backUserService.changeUser(backUser);
    }

    //查询重复名
    @ResponseBody
    @GetMapping("/isName/{userName}")
    public boolean isName(@PathVariable("userName")String userName){
        return backUserService.isName(userName);
    }

    //修改密码
    @GetMapping("/changePwd/{pwd}")
    @ResponseBody
    public boolean changePwd(@PathVariable("pwd")String pwd,HttpSession session){
        session.getAttribute("backUser");
        return backUserService.changePwd(pwd,((BackUser)session.getAttribute("adminUser")).getId());
    }
}
