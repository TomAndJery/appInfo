package top.xiaoge.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import top.xiaoge.pojo.DevUser;
import top.xiaoge.service.DevUserService;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

//登陆
@Controller
@RequestMapping("/admin")
public class DevUserController {
    @Autowired
    DevUserService devUserService;

    //管理员登陆页面
    @GetMapping("")
    public String DevUserLoginPage(){
        return "SysPage/login";
    }
    //用户管理页面 DevuserLists.html
    //App审核页面 appLists.html
    //修改密码页面 changePwd.html
    @GetMapping("/pages/{pageName}")
    public String htmlPages(@PathVariable("pageName")String pageName){
        return "SysPage/"+pageName.substring(0,pageName.lastIndexOf('.'));
    }

    //登陆
    @PostMapping("/login")
    @ResponseBody
    public boolean login(String devCode,String devPwd,HttpSession session){
        boolean res = false;
        DevUser devUser = devUserService.adminLogin(devCode, devPwd);
        if(devUser!=null){
            session.setAttribute("adminUser",devUser);
            res = true;
        }
        return res;
    }


    //检测是否重复名
    @GetMapping("/isCode/{code}")
    @ResponseBody
    public boolean isCode(@PathVariable("code") String code){
        return devUserService.isDevCode(code);
    }

    //添加管理员
    @PostMapping("/addDevUser")
    @ResponseBody
    public boolean addUser(DevUser devUser){
        return devUserService.addDevUser(devUser);
    }

    //删除管理员
    @GetMapping("/delUser/{id}")
    @ResponseBody
    public boolean delUser(@PathVariable("id") Integer id){
        return devUserService.delDevUser(id);
    }

    //修改管理员信息
    @PostMapping("/changeUser")
    @ResponseBody
    public boolean changeUser(DevUser devUser){
        return devUserService.changeDevUser(devUser);
    }
    //获取分页数据
    @GetMapping("/getData")
    @ResponseBody
    public HashMap<String,Object> getDevUserLimit(Integer page,Integer limit,String devCode,String username){
        return devUserService.getDevUserLists(page, limit, devCode, username);
    }

    //退出系统
    @GetMapping("/exitSys")
    public void exitSys(HttpSession session, HttpServletResponse response) throws IOException {
        session.removeAttribute("adminUser");
        response.sendRedirect("/login.html");
    }

    //修改密码
    @PostMapping("/changePwd/{devPwd}")
    @ResponseBody
    public boolean changePwd(DevUser devUser,HttpSession session){
        devUser.setId(((DevUser)session.getAttribute("adminUser")).getId());
        return devUserService.changeDevUser(devUser);
    }





}
