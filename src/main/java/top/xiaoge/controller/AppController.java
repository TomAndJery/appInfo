package top.xiaoge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import top.xiaoge.pojo.*;
import top.xiaoge.service.AppService;
import top.xiaoge.service.SortOneService;

import javax.servlet.ServletSecurityElement;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/app")
public class AppController {

    @Autowired
    AppService appService;

    @Autowired
    SortOneService sortOneService;

    @Value("${spring.servlet.multipart.location}")
    private String path;

    //分页app数据
    @RequestMapping("/getUserAppLists")
    @ResponseBody
    public HashMap<String,Object> getUserAppLists(Integer page, Integer limit, String appName, Integer type, Integer sortOne, Integer examine, HttpSession session){
        Integer userId = null;
        try{
            userId =  ((BackUser)session.getAttribute("adminUser")).getId();
        }catch (ClassCastException cce){
            System.out.println("类型转换错误!"+cce.getMessage());
            return appService.getUserApp(userId, page, limit, appName, type, sortOne, examine);
        }
        return appService.getUserApp(userId, page, limit, appName, type, sortOne, examine);
    }

    //获取所有分类
    @RequestMapping("/getAllSortOne")
    @ResponseBody
    public List<SortOne> getAllSortOne(){
        return sortOneService.getAllSortOne();
    }

    //审核app [1.通过 2.未审核 3.审核不通过]
    @RequestMapping("/examine")
    @ResponseBody
    public boolean examineApp(App app,HttpSession session){
        app.setLookerId(((DevUser)session.getAttribute("adminUser")).getId());
        if(app.getExamine()==3){
            app.setType(0);
        }
        return appService.changeApp(app);
    }

    //上架/下架
    @RequestMapping("/appud")
    @ResponseBody
    public boolean appud(App app){
        if(app.getType()==1){
            app.setIssueTime(new Date());
        }else if(app.getType()==0){
            app.setUnissureTime(new Date());
        }
        return appService.changeApp(app);
    }

    //删除app
    @RequestMapping("/delApp/{id}")
    @ResponseBody
    public boolean delApp(@PathVariable("id") Integer id){
        return appService.delApp(id);
    }

    //添加App
    @PostMapping("/addApp")
    @ResponseBody
    public boolean addApp(MultipartFile file,String version, App app, HttpSession session, HttpServletRequest request) throws IOException {
        boolean flag = false;
        //如果文件不是空
        if(!file.isEmpty()){
            app.setUserId(((BackUser)session.getAttribute("adminUser")).getId());
            app.setApkSize(((file.getSize()/1024)/1024));
            try {
//                String path = "C:\\upload\\";
                File pathFile = new File(path);
                if(!pathFile.exists()){
                    pathFile.mkdirs();
                }
                String fileName = file.getOriginalFilename();
                String suffixName = fileName.substring(fileName.lastIndexOf('.'));
                String prefixName = fileName.substring(0,fileName.lastIndexOf('.'));
                String newFileName = prefixName+UUID.randomUUID().toString().substring(8, 13)+suffixName;
                //保存文件
                file.transferTo(Paths.get(new File(path + newFileName)+""));
                app.setCreateDate(new Date());
//                app.setApkUrl("http://localhost:"+request.getServerPort()+"/app/down/"+newFileName);
                app.setApkUrl("http://localhost:"+request.getServerPort()+"/apks/"+newFileName);
                flag = appService.addApp(app,version);
            }catch (Exception e){
                System.out.println(e.getMessage());
                flag = false;
            }
        }
        return flag;
    }

    //下载个数增加
    @PostMapping("/addCount")
    @ResponseBody
    public void addCount(App app){
        //还是去前端直接赋值吧 =_=!
        appService.changeApp(app);
    }

    //下载路径映射
    @RequestMapping("/down/{apkName}")
    public String getFile(@PathVariable("apkName") String appName){
        return "redirect:/apks/"+appName;
    }

    //获取单个
    @RequestMapping("/getOne/{id}")
    @ResponseBody
    public App getOne(@PathVariable("id")Integer id){
        return appService.getOne(id);
    }

    //修改app信息
    @RequestMapping("/changeApp")
    @ResponseBody
    public boolean changeApp(App app){
        app.setModifyDate(new Date());
        app.setType(0);
        app.setExamine(2);
        return appService.changeApp(app);
    }








}
