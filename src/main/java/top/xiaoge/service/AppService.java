package top.xiaoge.service;
import top.xiaoge.pojo.App;
import top.xiaoge.pojo.Version;

import java.util.HashMap;


public interface AppService {
    //获取当前用户app
    HashMap<String,Object> getUserApp(Integer userId, Integer page, Integer limit, String appName, Integer type, Integer sortOne, Integer examine);
    //修改app信息
    boolean changeApp(App app);
    //删除App
    boolean delApp(Integer id);
    //添加App
    boolean addApp(App app,String version);
    //获取单个
    App getOne(Integer id);
}
