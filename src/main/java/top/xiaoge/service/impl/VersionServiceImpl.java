package top.xiaoge.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.xiaoge.mapper.VersionMapper;
import top.xiaoge.pojo.Version;
import top.xiaoge.service.VersionService;

import java.util.HashMap;


@Service
public class VersionServiceImpl implements VersionService {
    @Autowired
    VersionMapper versionMapper;

    //删除版本信息
    @Override
    public boolean delVersion(Integer appId, Integer id) {
        return versionMapper.delVersion(appId,id)>0;
    }

    @Override
    public HashMap<String,Object> getAllVersion(Integer appId) {
        HashMap<String,Object> map = new HashMap<>();
        map.put("code",0);
        map.put("msg","ok");
        map.put("count",versionMapper.getCount());
        map.put("data",versionMapper.getAllVersion(appId));
        return map;
    }

    @Override
    public boolean addVersion(Version version) {
        return versionMapper.addVersion(version)>0;
    }
}
