package top.xiaoge.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.xiaoge.mapper.BackUserMapper;
import top.xiaoge.pojo.BackUser;
import top.xiaoge.service.BackUserService;
import java.util.Date;
import java.util.HashMap;
@Service
public class BackUserServiceImpl implements BackUserService{

    @Autowired
    BackUserMapper backUserMapper;

    @Override
    public BackUser userLogin(String userCode, String userPwd) {
        return backUserMapper.userLogin(userCode,userPwd);
    }

    @Override
    public boolean changeUser(BackUser backUser) {
        backUser.setModifyDate(new Date());
        return backUserMapper.changeUser(backUser)>0;
    }

    @Override
    public boolean addUser(BackUser backUser) {
        backUser.setCreateDate(new Date());
        return backUserMapper.addUser(backUser)>0;
    }

    @Override
    public boolean delUser(Integer id) {
        return backUserMapper.delUser(id)>0;
    }

    @Override
    public boolean isName(String userCode) {
        return backUserMapper.isName(userCode)>0;
    }

    @Override
    public HashMap<String, Object> getUserLists(Integer page, Integer limit, String userCode, String userName) {
        HashMap<String,Object> map = new HashMap<>();
        map.put("code",0);
        map.put("msg","Ok！");
        map.put("count",backUserMapper.getCount());
        map.put("data",backUserMapper.getUserLists(page,limit,userCode,userName));
        return map;
    }

    @Override
    public boolean changePwd(String userPwd, Integer id) {
        System.out.println(userPwd+"|"+id);
        return backUserMapper.changePwd(userPwd,id)>0;
    }

}
