package top.xiaoge.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.xiaoge.mapper.AppMapper;
import top.xiaoge.mapper.VersionMapper;
import top.xiaoge.pojo.App;
import top.xiaoge.pojo.Version;
import top.xiaoge.service.AppService;
import java.util.HashMap;

@Service
public class AppServiceImpl implements AppService {

    @Autowired
    AppMapper appMapper;
    @Autowired
    VersionMapper versionMapper;

    //分页获取app列表
    @Override
    public HashMap<String, Object> getUserApp(Integer userId, Integer page, Integer limit, String appName, Integer type, Integer sortOne, Integer examine) {
        HashMap<String,Object> map = new HashMap<>();
        map.put("code",0);
        map.put("msg","ok！");
        map.put("count",appMapper.getAppCount(userId, appName, type, sortOne, examine));
        map.put("data",appMapper.getUserApp(userId, page, limit, appName, type, sortOne, examine));
        return map;
    }
    //修改app信息
    @Override
    public boolean changeApp(App app) {
        return appMapper.changeApp(app)>0;
    }

    //删除
    @Override
    @Transactional
    public boolean delApp(Integer id) {
        boolean flag = false;
        if(appMapper.delApp(id)>0){
            if(versionMapper.delVersion(id,null)>0){
                flag = true;
            }
        }
        return flag;
    }

    //添加App
    @Override
    @Transactional
    public boolean addApp(App app,String version) {
        boolean flag = false;
        if(appMapper.addApp(app)>0){
            Version ver = new Version(version,app.getId(),app.getContent(),app.getApkUrl(),app.getApkSize(),app.getCreateDate());
            if(versionMapper.addVersion(ver)>0){
                flag = true;
            }
        }
        return flag;
    }

    //获取单个
    @Override
    public App getOne(Integer id) {
        return appMapper.getOne(id);
    }


}
