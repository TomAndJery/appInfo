package top.xiaoge.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.xiaoge.mapper.DevUserMapper;
import top.xiaoge.pojo.DevUser;
import top.xiaoge.service.DevUserService;
import java.util.Date;
import java.util.HashMap;

@Service
public class DevUserServiceImpl implements DevUserService {
    @Autowired
    DevUserMapper devUserMapper;
    @Override
    public DevUser adminLogin(String devCode, String devPwd) {
        return devUserMapper.adminLogin(devCode,devPwd);
    }
    @Override
    public boolean changeDevUser(DevUser devUser) {
        return devUserMapper.changeDevUser(devUser)>0;
    }

    @Override
    public boolean addDevUser(DevUser devUser) {
        devUser.setCreateDate(new Date());
        return devUserMapper.addDevUser(devUser)>0;
    }

    @Override
    public boolean delDevUser(Integer id) {
        return devUserMapper.getCount()<=1?false:devUserMapper.delDevUser(id)>0;
    }

    @Override
    public boolean isDevCode(String devCode) {
        return devUserMapper.isDevCode(devCode)>0;
    }

    @Override
    public HashMap<String,Object> getDevUserLists(Integer page, Integer limit, String devCode, String devName) {
        HashMap<String,Object> map = new HashMap<>();
        map.put("code",0);
        map.put("msg","Ok！");
        map.put("count",devUserMapper.getCount());
        map.put("data",devUserMapper.getDevUserLists(page,limit,devCode,devName));
        return map;
    }
}
