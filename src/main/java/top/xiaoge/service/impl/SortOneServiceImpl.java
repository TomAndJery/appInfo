package top.xiaoge.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.xiaoge.mapper.SortOneMapper;
import top.xiaoge.pojo.SortOne;
import top.xiaoge.service.SortOneService;

import java.util.List;

@Service
public class SortOneServiceImpl implements SortOneService {

    @Autowired
    SortOneMapper sortOneMapper;

    @Override
    public List<SortOne> getAllSortOne() {
        return sortOneMapper.getAllSortOne();
    }
}
