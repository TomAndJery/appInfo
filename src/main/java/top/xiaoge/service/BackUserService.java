package top.xiaoge.service;

import org.apache.ibatis.annotations.Param;
import top.xiaoge.pojo.BackUser;

import java.util.HashMap;
import java.util.List;

public interface BackUserService {
    //登录
    BackUser userLogin(String userCode,String userPwd);
    //修改
    boolean changeUser(BackUser backUser);
    //添加
    boolean addUser(BackUser backUser);
    //删除
    boolean delUser(Integer id);
    //用户名查重
    boolean isName(String userCode);
    //分页查询数据
    HashMap<String,Object> getUserLists(Integer page, Integer limit, String userCode, String userName);
    //修改密码
    boolean changePwd(String userPwd,Integer id);
}
