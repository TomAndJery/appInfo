package top.xiaoge.service;
import top.xiaoge.pojo.DevUser;

import java.util.HashMap;
import java.util.List;

public interface DevUserService {
    //登录
    DevUser adminLogin(String devCode,String devPwd);
    //修改
    boolean changeDevUser(DevUser devUser);
    //添加
    boolean addDevUser(DevUser devUser);
    //删除
    boolean delDevUser(Integer id);
    //用户名查重
    boolean isDevCode(String devCode);
    //分页查询数据
    HashMap<String,Object> getDevUserLists(Integer page, Integer limit, String devCode, String devName);

}
