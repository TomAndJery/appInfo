package top.xiaoge.service;


import top.xiaoge.pojo.Version;

import java.util.HashMap;

public interface VersionService {
    //删除版本信息
    public boolean delVersion(Integer appId,Integer id);

    //查询所有版本
    HashMap<String,Object> getAllVersion(Integer appId);

    //添加
    boolean addVersion(Version versiton);
}
