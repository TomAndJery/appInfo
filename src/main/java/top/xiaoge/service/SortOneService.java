package top.xiaoge.service;

import top.xiaoge.mapper.SortOneMapper;
import top.xiaoge.pojo.SortOne;

import java.util.List;

public interface SortOneService {
    //获取所有分类
    List<SortOne> getAllSortOne();
}
