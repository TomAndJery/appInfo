package top.xiaoge.pojo;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class App {
    private Integer id;
    private String appName;
    private Integer userId;
    private Integer lookerId;
    private Integer type;
    private Integer sortOne;
    private Integer sortTwo;
    private Integer sortThree;
    private String content;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date issueTime;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date unissureTime;
    private double apkSize;
    private String apkUrl;
    private Integer apkCount;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date createDate;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date modifyDate;
    private Integer examine;

    private DevUser devUser;
    private SortOne sort;

}
