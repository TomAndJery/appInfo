package top.xiaoge.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.Date;


@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Version {
    private Integer id;
    private String version;
    private Integer app_id;
    private String content;
    private String apkUrl;
    private double apkSize;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date createDate;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date modifyDate;

    public Version(String version,Integer app_id,String content,String apkUrl,double apkSize,Date createDate){
        this.version = version;
        this.app_id = app_id;
        this.content = content;
        this.apkUrl = apkUrl;
        this.apkSize = apkSize;
        this.createDate = createDate;
    }
}
