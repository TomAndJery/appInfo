package top.xiaoge.pojo;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.Date;
@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BackUser {
  private Integer id;
  private String userCode;
  private String userPwd;
  private String userName;
  private Integer createBy;
  @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
  private Date createDate;
  private String phone;
  private Integer modifyBy;
  @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
  private Date modifyDate;

}
