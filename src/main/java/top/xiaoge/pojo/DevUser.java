package top.xiaoge.pojo;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DevUser {
  private Integer id;
  private String devCode;
  private String devPwd;
  private String devName;
  private String devEmail;
  private String devPhone;
  @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
  private Date createDate;
}
