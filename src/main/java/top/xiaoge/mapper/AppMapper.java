package top.xiaoge.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import top.xiaoge.pojo.App;

import java.util.List;

@Mapper
@Repository
public interface AppMapper {
    //获取当前用户app
    List<App> getUserApp(@Param("userId") Integer userId, @Param("page")Integer page,@Param("limit")Integer limit,@Param("appName")String appName,@Param("type")Integer type,@Param("sortOne")Integer sortOne,@Param("examine")Integer examine);
    //获取当前用户app总数
    int getAppCount(@Param("userId") Integer userId,@Param("appName")String appName,@Param("type")Integer type,@Param("sortOne")Integer sortOne,@Param("examine")Integer examine);
    //修改app
    int changeApp(App app);
    //删除App
    int delApp(Integer id);
    //添加App
    int addApp(App app);
    //获取单个
    App getOne(Integer id);
}
