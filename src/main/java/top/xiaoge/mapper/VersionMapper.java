package top.xiaoge.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import top.xiaoge.pojo.Version;

import java.util.List;
@Mapper
@Repository
public interface VersionMapper {
    //删除版本
    int delVersion(@Param("appId") Integer appId,@Param("id")Integer id);
    //添加版本
    int addVersion(Version versiton);
    //查询所有版本
    List<Version> getAllVersion(Integer appId);
    //获取数据量
    int getCount();

}
