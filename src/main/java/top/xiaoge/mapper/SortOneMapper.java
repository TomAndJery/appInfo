package top.xiaoge.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import top.xiaoge.pojo.SortOne;
import java.util.List;

@Mapper
@Repository
public interface SortOneMapper {
    //获取所有分类
    List<SortOne> getAllSortOne();
}
