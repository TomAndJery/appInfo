package top.xiaoge.mapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import top.xiaoge.pojo.BackUser;
import top.xiaoge.pojo.DevUser;
import java.util.List;

@Mapper
@Repository
public interface BackUserMapper {
    //登录
    BackUser userLogin(@Param("userCode") String userCode, @Param("userPwd") String userPwd);
    //修改
    int changeUser(BackUser backUser);
    //添加
    int addUser(BackUser backUser);
    //删除
    int delUser(Integer id);
    //用户名查重
    int isName(String userCode);
    //分页查询数据
    List<BackUser> getUserLists(@Param("page") Integer page, @Param("limit") Integer limit, @Param("userCode")String userCode, @Param("userName")String userName);
    //获取数据总量
    int getCount();
    //修改密码
    int changePwd(@Param("userPwd")String userPwd,@Param("id")Integer id);

}
