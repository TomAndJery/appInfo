package top.xiaoge.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import top.xiaoge.pojo.DevUser;
import java.util.List;

@Mapper
@Repository
public interface DevUserMapper {
    //登录
    DevUser adminLogin(@Param("devCode") String devCode,@Param("devPwd") String devPwd);
    //修改
    int changeDevUser(DevUser devUser);
    //添加
    int addDevUser(DevUser devUser);
    //删除
    int delDevUser(Integer id);
    //用户名查重
    int isDevCode(String devCode);
    //分页查询数据
    List<DevUser> getDevUserLists(@Param("page") Integer page,@Param("limit") Integer limit,@Param("devCode")String devCode,@Param("devName")String devName);
    //获取数据总量
    int getCount();
}
