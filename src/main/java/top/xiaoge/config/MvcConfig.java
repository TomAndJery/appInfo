package top.xiaoge.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Value("${spring.servlet.multipart.location}")
    private String path;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/admin","/admin/login","/css/**","/fonts/**","/images/**","/js/**","/lib/**","/login.html",
                        "/user/userLogin","/user/addBackUser","/","/user/isName/*"
                );
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("").setViewName("exploitationPage/login");
        registry.addViewController("/login.html").setViewName("exploitationPage/login");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        System.out.println("代理成功，"+path);
        registry.addResourceHandler("/apks/**").addResourceLocations("file:"+path);
    }
}
