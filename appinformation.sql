/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : 127.0.0.1:3306
 Source Schema         : appinformation

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 18/09/2022 00:23:51
*/
CREATE DATABASE /*!32312 IF NOT EXISTS*/`appinformation` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `appinformation`;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for app
-- ----------------------------
DROP TABLE IF EXISTS `app`;
CREATE TABLE `app`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `appName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用名称',
  `userId` int NOT NULL COMMENT '开发者id',
  `lookerId` int NULL DEFAULT NULL COMMENT '审核员id',
  `type` int NULL DEFAULT 0 COMMENT '状态',
  `sortOne` int NULL DEFAULT NULL COMMENT '一级分类',
  `sortTwo` int NULL DEFAULT NULL COMMENT '二级分类',
  `sortThree` int NULL DEFAULT NULL COMMENT '三级分类',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'app简介',
  `issueTime` datetime NULL DEFAULT NULL COMMENT '上架时间',
  `unissureTime` datetime NULL DEFAULT NULL COMMENT '下架时间',
  `apkSize` double NULL DEFAULT NULL COMMENT 'app大小',
  `apkUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'apk路径',
  `apkCount` int NOT NULL DEFAULT 0 COMMENT '下载次数',
  `createDate` datetime NOT NULL COMMENT '创建时间',
  `modifyDate` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `examine` int NULL DEFAULT 2 COMMENT '审核状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app
-- ----------------------------
INSERT INTO `app` VALUES (18, 'ttt', 1, 1, 1, 1, NULL, NULL, '111', '2022-09-18 00:20:26', '2022-09-18 00:20:21', 1, 'http://localhost:8086/apks/喜洋洋-40bd.apk', 2, '2022-09-18 00:14:52', NULL, 1);
INSERT INTO `app` VALUES (19, '4', 1, NULL, 0, 2, NULL, NULL, '1243', NULL, NULL, 1, 'http://localhost:8086/apks/喜洋洋-c1b4.apk', 0, '2022-09-18 00:21:04', NULL, 2);

-- ----------------------------
-- Table structure for back_user
-- ----------------------------
DROP TABLE IF EXISTS `back_user`;
CREATE TABLE `back_user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `userCode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userPwd` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createBy` int NULL DEFAULT NULL,
  `createDate` datetime NULL DEFAULT NULL,
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `modifyBy` int NULL DEFAULT NULL,
  `modifyDate` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of back_user
-- ----------------------------
INSERT INTO `back_user` VALUES (1, 'test', '123456', '测试用户', NULL, '2022-09-18 16:57:28', '17656312455', 2, '2022-09-18 16:01:20');
INSERT INTO `back_user` VALUES (10, 'zhuceyonghu', 'zhuce123', '注册用户', NULL, '2022-09-18 00:22:35', '12312312312', NULL, NULL);

-- ----------------------------
-- Table structure for dev_user
-- ----------------------------
DROP TABLE IF EXISTS `dev_user`;
CREATE TABLE `dev_user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `devCode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `devPwd` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `devName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `devEmail` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `devPhone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `createDate` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dev_user
-- ----------------------------
INSERT INTO `dev_user` VALUES (1, 'admin', '123456', '系统管理员', 'admin.top@qq.com', '17653333455', '2022-09-18 15:44:08');

-- ----------------------------
-- Table structure for sortone
-- ----------------------------
DROP TABLE IF EXISTS `sortone`;
CREATE TABLE `sortone`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `sortOne` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '一级分类',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sortone
-- ----------------------------
INSERT INTO `sortone` VALUES (1, '休闲娱乐');
INSERT INTO `sortone` VALUES (2, '聊天交友');
INSERT INTO `sortone` VALUES (3, '系统工具');
INSERT INTO `sortone` VALUES (4, '益智游戏');
INSERT INTO `sortone` VALUES (5, '运动健身');
INSERT INTO `sortone` VALUES (6, '赛车竞速');
INSERT INTO `sortone` VALUES (7, '手游辅助');
INSERT INTO `sortone` VALUES (8, '脚本外挂');
INSERT INTO `sortone` VALUES (9, '时尚购物');
INSERT INTO `sortone` VALUES (10, '摄影摄像');
INSERT INTO `sortone` VALUES (11, '学习教育');
INSERT INTO `sortone` VALUES (12, '金融理财');
INSERT INTO `sortone` VALUES (13, '新闻资讯');
INSERT INTO `sortone` VALUES (14, '医疗健康');
INSERT INTO `sortone` VALUES (15, '效率办公');

-- ----------------------------
-- Table structure for version
-- ----------------------------
DROP TABLE IF EXISTS `version`;
CREATE TABLE `version`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '版本id',
  `version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '版本编号',
  `app_id` int NOT NULL COMMENT 'appId',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本介绍',
  `apkUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'apk连接',
  `apkSize` double(11, 2) NOT NULL COMMENT 'apk大小',
  `createDate` datetime NOT NULL COMMENT '创建时间',
  `modifyDate` datetime NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `v_id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of version
-- ----------------------------
INSERT INTO `version` VALUES (17, '6', 18, '111', 'http://localhost:8086/app/down/喜洋洋-40bd.apk', 1.00, '2022-09-18 00:14:52', NULL);
INSERT INTO `version` VALUES (18, '666', 18, '345', 'http://localhost:8086/app/down/喜洋洋-5977.apk', 1.00, '2022-09-18 00:15:07', NULL);
INSERT INTO `version` VALUES (19, '12', 19, '1243', 'http://localhost:8086/apks/喜洋洋-c1b4.apk', 1.00, '2022-09-18 00:21:04', NULL);

SET FOREIGN_KEY_CHECKS = 1;
